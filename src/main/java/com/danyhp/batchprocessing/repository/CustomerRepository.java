package com.danyhp.batchprocessing.repository;

import com.danyhp.batchprocessing.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author U067291 - Dany Hasiando Panggabean on 08-Mar-24 11:46
 * @project batch-processing
 */
public interface CustomerRepository extends JpaRepository<Customer, Integer> {
}
