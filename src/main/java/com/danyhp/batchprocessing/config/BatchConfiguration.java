package com.danyhp.batchprocessing.config;

import com.danyhp.batchprocessing.entity.Customer;
import com.danyhp.batchprocessing.repository.CustomerRepository;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.avro.AvroItemReader;
import org.springframework.batch.item.avro.AvroItemWriter;
import org.springframework.batch.item.avro.builder.AvroItemReaderBuilder;
import org.springframework.batch.item.avro.builder.AvroItemWriterBuilder;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.FlatFileItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.data.domain.Sort;
import org.springframework.transaction.PlatformTransactionManager;

import java.util.HashMap;

/**
 * @author U067291 - Dany Hasiando Panggabean on 08-Mar-24 11:32
 * @project batch-processing
 */

@Configuration
@AllArgsConstructor
public class BatchConfiguration {

    private CustomerRepository customerRepository;

    @Bean
    public CustomerProcessor processor() {
        return new CustomerProcessor();
    }

    @Bean
    public FlatFileItemReader<Customer> reader() {
        return new FlatFileItemReaderBuilder<Customer>()
                .name("customerItemReader")
                .resource(new ClassPathResource("customers.csv"))
                .delimited()
                .names("id", "firstName", "lastName", "email", "gender", "contactNo", "country", "dob")
                .fieldSetMapper(new BeanWrapperFieldSetMapper<>() {{
                    setTargetType(Customer.class);
                }})
                .linesToSkip(1)
                .build();
    }

    @Bean
    public FlatFileItemWriter<Customer> customerWriter() {
        return new FlatFileItemWriterBuilder<Customer>()
                .name("customerItemWriter")
                .resource(new FileSystemResource("/dany/customers-generated.txt"))
                .delimited()
                .delimiter("|~|")
                .names("id", "firstName", "lastName", "email", "gender", "contactNo", "country", "dob")
                .headerCallback(writer -> writer.write("id|~|firstName|~|lastName|~|email|~|gender|~|contactNo|~|country|~|dob"))
                .build();
    }

    @Bean
    public AvroItemWriter<Customer> customerAvroWriter() {
        return new AvroItemWriterBuilder<Customer>()
                .name("customerAvroWriter")
                .resource(new FileSystemResource("/dany/customers-generated.txt"))
                .type(Customer.class)
                .schema(new ClassPathResource("avro/customer-schema.avsc"))
                .build();
    }

    @Bean
    public AvroItemReader<Customer> customerAvroReader() {
        return new AvroItemReaderBuilder<Customer>()
                .name("customerAvroReader")
                .resource(new FileSystemResource("/dany/customers-generated.avro"))
//                .schema(new ClassPathResource("avro/customer-schema.avsc"))
                .type(Customer.class)
                .build();
    }

    @Bean
    public RepositoryItemWriter<Customer> writer() {
        RepositoryItemWriter<Customer> writer = new RepositoryItemWriter<>();
        writer.setRepository(customerRepository);
        writer.setMethodName("save");
        return writer;
    }

    @Bean
    public RepositoryItemReader<Customer> customerReader() {
        RepositoryItemReader<Customer> reader = new RepositoryItemReader<>();
        HashMap<String, Sort.Direction> sorts = new HashMap<>();
        sorts.put("id", Sort.Direction.ASC);

        reader.setRepository(customerRepository);
        reader.setMethodName("findAll");
        reader.setSort(sorts);
        return reader;
    }

    @Bean
    Job importUserJob(JobRepository jobRepository, Step step1, Step step2, Step step3, JobCompletionNotificationListener listener) {
        return new JobBuilder("importCustomerJob", jobRepository)
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(step2)
                .next(step3)
                .end()
                .build();
    }

    @Bean
    public Step step1(JobRepository jobRepository, PlatformTransactionManager transactionManager, CustomerProcessor processor, FlatFileItemReader<Customer> reader, RepositoryItemWriter<Customer> writer) {
        return new StepBuilder("step1", jobRepository)
                .<Customer, Customer>chunk(100, transactionManager)
                .reader(reader)
                .processor(processor)
                .writer(writer)
//                .taskExecutor(taskExecutor())
                .build();

    }

    @Bean Step step2(JobRepository jobRepository, PlatformTransactionManager transactionManager, CustomerProcessor processor, AvroItemWriter<Customer> writer, RepositoryItemReader<Customer> reader) {
        return new StepBuilder("step2", jobRepository)
                .<Customer, Customer>chunk(100, transactionManager)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .build();
    }

    @Bean Step step3(JobRepository jobRepository, PlatformTransactionManager transactionManager, AvroItemReader<Customer> reader, CustomerProcessor processor, FlatFileItemWriter<Customer> writer) {

//        reader.setEmbeddedSchema(false);
        return new StepBuilder("step3", jobRepository)
                .<Customer, Customer>chunk(100, transactionManager)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .build();
    }


}
