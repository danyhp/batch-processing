package com.danyhp.batchprocessing.config;

import com.danyhp.batchprocessing.entity.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

/**
 * @author U067291 - Dany Hasiando Panggabean on 08-Mar-24 11:43
 * @project batch-processing
 */
public class CustomerProcessor implements ItemProcessor<Customer, Customer> {

    private static final Logger log = LoggerFactory.getLogger(CustomerProcessor.class);

    @Override
    public Customer process(Customer customer) throws Exception {

        log.info("Processing (" + customer + ")");

        return customer;
    }
}
